This is common library provide basic functions for Xilinx/MicroZed bare metal application. It��s not an independent application but should be provided as a dependent project to other application projects.

Current functions include serial command interpretation, qspi flash read/write/erase functions, GPIO related functions such as initialization, read/write, CRC calculation.  
